# README #

1. A way of recording perf log based on annotation.
2. Support CCS autoconfiguration(`@CCSEnable`,`@CCSMappingCache`)
3. Extend Springboot　Property source, support loading all classpath properties.
   
## User Guide ##

> Import Maven dependency

``` xml
   <dependency>
        <groupId>com.derbysoft.warrior</groupId>
        <artifactId>common-extra</artifactId>
        <version>1.3.7</version>
   </dependency>
```


> Gradle dependency

*Note: Spring version 5.3.3, if your springversion is inconsistent, you maybe need exclude related jar.

*Note: Spring is 4.x.x，please use `compile group: 'com.derbysoft.warrior', name: 'common-extra',version: "1.1.0-SNAPSHOT`

*Default, perf version is v1.

> SpringFramework Application config
```xml
    <import resource="classpath:com/derbysoft/warrior/common/extra/log/application-perflog.xml"/>
```

> SpringBoot config as follows
````properties
perf.annotation.enable=true
#'v1', 'v2' or 'v1,v2'
perf.version=v2
````

If your application is not Springboot and need to upgrade perfV2, maybe you need to init perflog configuration before application start (By implement ServletContextListener OR ServletContainerInitializer). 
As below:
```java
//init kafka topic 
String topic = "perf_test_raw";
String appName = "test-app";
PerfLogBuilder.init(topic, appName);

// Select perf log version(PerfV2MessageBuilder, PerfV1MessageBuilder) and set loggerName  ('PERF_LOGGER' same as logbakc.xml)
PerfTracer.setRecorders(List.of(new DefaultPerfLogRecorder(new PerfV2MessageBuilder(), "PERF_LOGGER")));

//Example
@WebListener
public class PerfLogConfiguationInitializer implements ServletContextListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(PerfLogConfiguationInitializer.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        PerfLogBuilder.init("perf_jtb-reservation_raw", "jtb-reservation");
        PerfTracer.setRecorders(List.of(new DefaultPerfLogRecorder(new PerfV2MessageBuilder(), "PERF_LOGGER")));
    }
}


```


# Usage #

### @Perf ###

```java
    @Perf(action = PerfConst.GET_AVAILABILITY, payloadTracer = AvailabilityPayloadTracer.class)
    public HotelAvailResponse getAvailability(HotelAvailRequest hotelAvailRequest) {
            return hotelBuyerRemoteService.getAvailability(request);
    }
```
Declaration ``@Perf` on the entrance, specify the payloadTracer. (default the `NoOpPayloadTracer` will be used, `errorTracer` is optional)

### Example ###
```java
public class AvailabilityPayloadTracer extends AbstractPerfPayloadTracer {

    @Override
    protected void perf(Object[] arguments) {
        HotelAvailRequest request = (HotelAvailRequest) arguments[0];

        PerfTracer.get().message(PerfConst.Parameter.SUPPLIER, Constants.HYATT);
        PerfTracer.get().message(PerfConst.Parameter.CHANNEL, request.getHeader().getSource());
        PerfTracer.get().message(PerfConst.Parameter.PROCESS_SUPPLIER, "OTA_HotelAvail");
        PerfTracer.get().message(PerfConst.Parameter.ECHO_TOKEN_DERBY, request.getHeader().getTaskId());
    }

    @Override
    protected void perf(Object result) {
        HotelAvailResponse response = (HotelAvailResponse) result;
        //availability.result
        ErrorDTO error = response.getError();
        if (error != null) {
            PerfTracer.get().message(PerfConst.Parameter.ERR_CODE, error.getCode());
            PerfTracer.get().message(PerfConst.Parameter.ERR_MSG, error.getMessage());
            PerfTracer.get().message(PerfConst.Parameter.ERR_SOURCE, error.getSource());
            PerfTracer.get().message(PerfConst.Parameter.PROCESS_RESULT, ProcessResult.FAIL);
        } else {
            List<HotelAvailRoomStayDTO> hotelAvailRoomStayDTOS = Optional.of(response).map(HotelAvailResponse::getHotelAvailRS)
                    .map(HotelAvailRS::getHotelAvailRoomStaysList)
                    .orElse(null);
            PerfTracer.get().message(PerfConst.Parameter.AVAILABILITY_RESULT, CollectionUtils.isEmpty(hotelAvailRoomStayDTOS) ? "NoAvail" : "Avail");
        }
    }
}
```
### @PerfDuration ###
```java
    @PerfDuration(action = "query", target = "b2b.gateway")
    public OTAHotelAvailRS liveCheck(OTAHotelAvailRQ hotelAvailRQ) {
        return send(hotelAvailRQ, null);
    }
```
Statistics execution time, and finally key is `<query.b2b.gateway.duration=1000>`

### @PerfKeys and @PerfKey ###

```java
//  Extract information from method parameters
    @PerfKeys({
            @PerfKey(name = "channel", index = "[0].getRawMessageRS.channel"),
            @PerfKey(name = "supplier", index = "[0].getRawMessageRS.provider"),
            @PerfKey(name = "hotel.supplier", index = "[0].getRawMessageRS.hotel"),
            @PerfKey(name = "start.date", index = "[0].getRawMessageRS.messageMeta.ariStartDate"),
            @PerfKey(name = "end.date", index = "[0].getRawMessageRS.messageMeta.ariEndDate"),
            @PerfKey(name = "request.type", index = "[0].getRawMessageRS.messageMeta.ariType")
    })
    public void save(GetRawMessageResponse getRawMessageResponse) {
        saveRawMessage(getRawMessageResponse);
    }

    @PerfKeys(value = {
            @PerfKey(name = "channel", index = "[0]"),
            @PerfKey(name = "supplier", index = "[1]"),
            @PerfKey(name = "request.type", constValue = "OTA"),
            @PerfKey(name = "return", target = PerfKey.TARGET_TYPE_RETURN),
    })
    public String doSome(String channel, String supplier) {
            return "dosome";
    }
```
'[0]' Represents the first parameter ，'[1]'Represents the second parameter


```java
//  Record the returned value 
    @PerfKeys(value = {
            @PerfKey(name = "user.name", index = "name", target = PerfKey.TARGET_TYPE_RETURN)
    })
    public User getUser() {
        return new User("getUser", 99);
    }
```
```java
//  custom payloadTracer
    @PerfKeys(payloadTracer = AvailabilityPayloadTracer.class)
    public HotelAvailResponse getAvail(HotelAvailRequest hotelAvailRequest) {
        return new HotelAvailResponse(new ErrorDTO("System", "error", "test"));
    }
```

### @Stream ###
```java
    @Stream(payloadTracer = AvailabilityStreamTracer.class)
    public HotelAvailResponse getAvailability(HotelAvailRequest hotelAvailRequest) {
         return hotelBuyerRemoteService.getAvailability(request);
    }
```
```java
public class AvailabilityStreamTracer implements PayloadTracer {
    private static final Logger LOGGER = LoggerFactory.getLogger("STREAM_LOG");
    @Override
    public void trace(Object[] arguments, Object result) {
        HotelAvailRequest request = (HotelAvailRequest) arguments[0];
        HotelAvailResponse response = (HotelAvailResponse) result;

        LOGGER.info(XStreamUtils.toXML(request));
        LOGGER.info(XStreamUtils.toXML(response));

    }
}
```

You have to be careful, It should be noted that the annotation `@PerfKeys`  will not take effect,as follows:

```java
   @Perf(action = "Book", payloadTracer = BookPayloadTracer.class)
    public Object Book(Object request) {
   		getUser(); // called inside the method   
    }
    
    @PerfKeys(value = {
            @PerfKey(name = "user.name", index = "name", target = PerfKey.TARGET_TYPE_RETURN)
    })
    public User getUser() {
        return new User("getUser", 99);
    }

```
Because proxied class can't not be called in a nested.



----------------
#### For some non editable class, eg: inside jar, we can define a BeanPostProcessor as follows: ####

```xml
    <bean class="com.derbysoft.warrior.common.extra.log.processor.StreamProxyBeanPostProcessor">
        <property name="streamBeanConfigs">
            <list>
                <bean class="com.derbysoft.warrior.common.extra.log.StreamBeanConfig">
                    <constructor-arg index="0" value="hotelAvailabilityRemoteService"/>  //which bean
                    <constructor-arg index="1" value="getAvailability"/>    //which method
                    <property name="payloadTracerClass" value="com.derbysoft.hyatt.storage.shop.proxy.log.AvailabilityStreamTracer"/>  //parameter capture
                </bean>
            </list>
        </property>
    </bean>
```
```java
    @Bean
    public PerfProxyBeanPostProcessor perfProxyBeanPostProcessor() {
        PerfBeanConfig hotelChangePerf = new PerfBeanConfig("hotelBuyerRemoteService");
        hotelChangePerf.addPointcut("getHotelChange", PerfConst.HOTEL_CHANGE, new HotelChangePerfLog());
        hotelChangePerf.addPointcut("getHotelChangeDates", PerfConst.HOTEL_CHANGE_DATES, new HotelDateChangePerfLog());

        PerfProxyBeanPostProcessor processor = new PerfProxyBeanPostProcessor();
        processor.setPerfBeanConfigs(List.of(hotelChangePerf));
        return processor;
    }
```

## CCS AutoConfiguration ##

#### @CCSEnable & @CCSMappingCache ####

`@CCSEnable` 会开启自动配置MappingCache以及CCS TopicConsumer相关类, 通过entityPackages来指定扫描哪个包下的实体类.

`@CCSMappingCache` 代表该类会被mapped成一个MappingCache，并且也会将该类注入到Spring容器中.


Example: 开启CCS自动注册, 且ccs 实体对象在 `com.derbysoft.hyatt.common.domain`目录下
```java
@SpringBootApplication
@CCSEnable(entityPackages = "com.derbysoft.hyatt.common.domain")
public class ShopApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ShopApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(ShopApplication.class, args);
    }
}
```

Example: HotelMealPlan 会被mapped成一个MappingCache的bean,且该bean的名字为hotelMealPlanCache
```java
package com.derbysoft.hyatt.common.domain;


@CCSMappingCache(topic = "${ccs.topic.hotel.meal.plan}", name = "hotelMealPlanCache")
public class HotelMealPlan implements MappingEntityDisabledField {
    public static final String ANY_RATE_PLAN = "*";

    public static final String DEFAULT_MEAL_CODE = "RO";

    private String hotel;

    private String ratePlan;

    private String mealPlan;

    private boolean disabled;
    //set and get

}
```
------
### Config file ###

默认情况下,springboot只会加载名字为 `application-{profile}.properties` 或者　`application-{profile}.yml(yaml)`文件（不支持`classpath:*.properties）, 
`common-config`模块扩展了配置文件的读取方式(`property.placeholder.location`指定)，可以读取某个目录下所有的配置文件，结合`maven`或者`gradle`工具可按目录(dev,demo,prd etc)来分环境打包

添加以下配置，即可加载classpath下所有配置文件.
```properties
property.placeholder.location=classpath:*.properties
```


