package com.derbysoft.warrior.common.extra.config;

import org.springframework.boot.ResourceBanner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.ByteArrayResource;

import java.nio.charset.StandardCharsets;

public class ApplicationConfigEnvironmentProcessor implements EnvironmentPostProcessor, Ordered {

    private static final String BANNER = "██████╗ ███████╗██████╗ ██████╗ ██╗   ██╗███████╗ ██████╗ ███████╗████████╗\n" +
            "██╔══██╗██╔════╝██╔══██╗██╔══██╗╚██╗ ██╔╝██╔════╝██╔═══██╗██╔════╝╚══██╔══╝\n" +
            "██║  ██║█████╗  ██████╔╝██████╔╝ ╚████╔╝ ███████╗██║   ██║█████╗     ██║\n" +
            "██║  ██║██╔══╝  ██╔══██╗██╔══██╗  ╚██╔╝  ╚════██║██║   ██║██╔══╝     ██║\n" +
            "██████╔╝███████╗██║  ██║██████╔╝   ██║   ███████║╚██████╔╝██║        ██║\n" +
            "╚═════╝ ╚══════╝╚═╝  ╚═╝╚═════╝    ╚═╝   ╚══════╝ ╚═════╝ ╚═╝        ╚═╝\n" +
            "\n" +
            "Spring Boot 版本：${spring-boot.version}";

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        application.addInitializers(new ApplicationConfigInitializer());
        if (environment.getProperty("derbySoft.banner", Boolean.class, true)) {
            application.setBanner(new ResourceBanner(new ByteArrayResource(BANNER.getBytes(StandardCharsets.UTF_8))));
        }
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 999;
    }
}
