package com.derbysoft.warrior.common.extra.log;

/**
 * @author quan.wu
 */
public interface PerfLogRecorder {

    String LOGGER_NAME_PERF = "PERF_LOGGER";
    String LOGGER_NAME_PERF_V2 = "PERF_LOGGER_V2";

    String record(PerfTracer tracer);

}
