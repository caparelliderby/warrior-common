package com.derbysoft.warrior.common.extra.log.advisor;

import com.derbysoft.warrior.common.extra.log.*;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;

/**
 * @author traitswu
 * @date 2021/3/10
 */
public class PerfInterceptor implements MethodInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(PerfInterceptor.class);
    private static final String EXPRESSION_PREFIX = "#{";
    private static final String EXPRESSION_SUFFIX = "}";

    @Override
    public Object invoke(final MethodInvocation invocation) throws Throwable {
        Perf perf = ReflectUtils.getMethodAnnotation(invocation, Perf.class);
        if (perf != null) {
            PerfErrorTracer errorHandler = ReflectUtils.newInstance(perf.errorTracer());
            PayloadTracer perfPayloadTracer = ReflectUtils.newInstance(perf.payloadTracer());
            String op = perf.action();
            if (isExpression(op)) {
                op = resolveValueNoException(invocation.getThis(), op);
            }
            return new PerfTemplate(op, errorHandler).invoke(() -> {
                Object[] arguments = null;
                Object result = null;
                try {
                    arguments = invocation.getArguments();
                    result = invokeWithDuration(invocation);
                    return result;
                } finally {
                    if (perfPayloadTracer != null) {
                        perfPayloadTracer.trace(arguments, result);
                    }
                }
            });
        } else {
            return invokeWithDuration(invocation);
        }
    }

    protected Object invokeWithDuration(final MethodInvocation invocation) throws Throwable {
        PerfDuration perfDuration = ReflectUtils.getMethodAnnotation(invocation, PerfDuration.class);
        if (perfDuration != null) {
            String target = perfDuration.target();
            String action = perfDuration.action();
            long startTimeMillis = System.currentTimeMillis();
            try {
                return invokeWithPerfKeys(invocation);
            } finally {
                long endTimeMillis = System.currentTimeMillis();
                PerfTracer.get().message(action + "." + target + ".duration", endTimeMillis - startTimeMillis);
            }
        } else {
            return invokeWithPerfKeys(invocation);
        }
    }

    protected Object invokeWithPerfKeys(MethodInvocation invocation) throws Throwable {
        Class<?> targetClass = AopUtils.getTargetClass(invocation.getThis());
        Method method = AopUtils.getMostSpecificMethod(invocation.getMethod(), targetClass);
        method.setAccessible(true);
        PerfKeys perfKeys = method.getAnnotation(PerfKeys.class);
        if (perfKeys != null) {
            Object result = null;
            Object[] arguments = null;
            try {
                arguments = invocation.getArguments();
                result = invocation.proceed();
                return result;
            } finally {
                PerfKey[] perfPairs = perfKeys.value();
                if (perfPairs.length > 0) {
                    for (PerfKey perfPair : perfPairs) {
                        try {
                            byte target = perfPair.target();
                            if (StringUtils.isNotBlank(perfPair.constValue()) || target == PerfKey.TARGET_TYPE_CONST) {
                                recordPerfConst(perfPair);
                            } else if (target == PerfKey.TARGET_TYPE_RETURN) {
                                recordPerf(perfPair, result);
                            } else if (target == PerfKey.TARGET_TYPE_PARAMS) {
                                recordPerf(perfPair, arguments);
                            } else {
                                throw new IllegalArgumentException("invalid perfkey target type:" + target);
                            }

                        } catch (Exception e) {
                            LOGGER.warn("record @PerfKey[" + perfPair.index() + "] fail:", e);
                        }
                    }
                }

                Class<? extends PayloadTracer> payloadTracer = perfKeys.payloadTracer();
                PayloadTracer tracer = payloadTracer.newInstance();
                tracer.trace(arguments, result);
            }
        } else {
            return invocation.proceed();
        }
    }

    private void recordPerf(PerfKey perfKey, Object object) throws Exception {
        String name = perfKey.name();
        String index = perfKey.index();
        String value = resolveValue(object, index);
        if (value != null) {
            PerfTracer.get().message(name, value);
        }
    }

    private void recordPerfConst(PerfKey perfKey) {
        String name = perfKey.name();
        String value = perfKey.constValue();
        if (value.trim().length() > 0) {
            PerfTracer.get().message(name, value);
        }
    }

    private String resolveValue(Object result, String index) throws Exception {
        if (result == null) {
            return null;
        } else if (result.getClass() == String.class) {
            return result.toString();
        } else if (result.getClass().isPrimitive()) {
            return String.valueOf(result);
        }
        Object propertyValue = PropertyUtils.getProperty(result, index);
        return propertyValue == null ? null : toString(propertyValue);
    }

    private String resolveValueNoException(Object result, String index) {
        try {
            String name = index.substring(EXPRESSION_PREFIX.length(), index.length() - EXPRESSION_SUFFIX.length());
            return resolveValue(result, name);
        } catch (Exception e) {
            LOGGER.error("resolveValue error:{},{}, {}", result.toString(), index, ExceptionUtils.getStackTrace(e));
            return result.toString();
        }
    }

    private String toString(Object val) {
        if (val == null) {
            return null;
        } else if (val.getClass().isPrimitive()) {
            return val.toString();
        } else if (val.getClass().isArray()) {
            return Arrays.toString((Object[]) val);
        } else {
            return val instanceof Collection ? Arrays.toString(((Collection) val).toArray()) : String.valueOf(val);
        }
    }

    private boolean isExpression(String op) {
        return op.startsWith(EXPRESSION_PREFIX) && op.endsWith(EXPRESSION_SUFFIX);
    }

}