package com.derbysoft.warrior.common.extra.config;

import com.derbysoft.common.util.perflog.v2.PerfLogBuilder;
import com.derbysoft.warrior.common.extra.log.DefaultPerfLogRecorder;
import com.derbysoft.warrior.common.extra.log.PerfLogRecorder;
import com.derbysoft.warrior.common.extra.log.PerfTracer;
import com.derbysoft.warrior.common.extra.log.PerfV2MessageBuilder;
import com.google.common.collect.ImmutableMap;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PerfLogConfigInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext>, Ordered {
    private static final String PERF_VERSION = "perf.version";
    private static final String APP_NAME = "application.name";
    private static final String LOG_KAFKA_TOPIC = "perf.kafka.topic";


    private final Map<String, PerfLogRecorder> versionRecords = ImmutableMap.of(
            "v1", new DefaultPerfLogRecorder(),
            "v2", new DefaultPerfLogRecorder(new PerfV2MessageBuilder(), DefaultPerfLogRecorder.LOGGER_NAME_PERF_V2)
    );

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        String versions = applicationContext.getEnvironment().getProperty(PERF_VERSION);
        if (StringUtils.hasText(versions)) {
            Set<String> versionSet = StringUtils.commaDelimitedListToSet(versions);
            configPerfLogRecorder(versionSet);
            if (versionSet.contains("v2")) {
                String appName = applicationContext.getEnvironment().getProperty(APP_NAME);
                String topic = applicationContext.getEnvironment().getProperty(LOG_KAFKA_TOPIC);
                PerfLogBuilder.init(topic, appName);
            }
        }

    }

    private void configPerfLogRecorder(Set<String> versions) {
        List<PerfLogRecorder> recorders = new ArrayList<>();
        for (String version : versions) {
            PerfLogRecorder recorder = versionRecords.get(version);
            if (recorder != null) {
                recorders.add(recorder);
            }
        }
        if (!recorders.isEmpty()) {
            PerfTracer.setRecorders(recorders);
        }
    }

    @Override
    public int getOrder() {
        return LOWEST_PRECEDENCE;
    }
}
