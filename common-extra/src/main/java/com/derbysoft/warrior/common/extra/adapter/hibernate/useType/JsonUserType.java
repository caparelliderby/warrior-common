package com.derbysoft.warrior.common.extra.adapter.hibernate.useType;

import com.alibaba.fastjson.JSON;
import com.derbysoft.common.hibernate.usertype.StringUserType;


/**
 * Notes:
 * Cann't use derbysoft-commons5, JSON deserialize and serialize are different
 */

public class JsonUserType extends StringUserType {

    @Override
    protected Object deserialize(String objectValue, Object owner) {
        return objectValue == null ? null : JSON.parseObject(objectValue, getObjectType());
    }

    @Override
    protected String serialize(Object object) {
        return object == null ? null : JSON.toJSONString(object);
    }

}