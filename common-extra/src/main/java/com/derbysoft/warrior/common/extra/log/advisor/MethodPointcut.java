package com.derbysoft.warrior.common.extra.log.advisor;

import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.aop.support.annotation.AnnotationMethodMatcher;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author traitswu
 * @date 2021/3/10
 */
public class MethodPointcut extends StaticMethodMatcherPointcut {

    private final List<AnnotationMethodMatcher> annotationMethodMatchers = new ArrayList<>();

    public MethodPointcut(Class<? extends Annotation>[] annotations) {
        for (Class<? extends Annotation> annotation : annotations) {
            annotationMethodMatchers.add(new AnnotationMethodMatcher(annotation));
        }
    }

    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        for (AnnotationMethodMatcher each : annotationMethodMatchers) {
            boolean matches = each.matches(method, targetClass);
            if (matches) {
                return true;
            }
        }

        return false;
    }

}
