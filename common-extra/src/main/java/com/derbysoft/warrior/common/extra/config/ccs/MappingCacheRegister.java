package com.derbysoft.warrior.common.extra.config.ccs;

import com.derbysoft.ccs.core.MappingCache;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.RuntimeBeanNameReference;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySourcesPropertyResolver;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.classreading.SimpleMetadataReaderFactory;
import org.springframework.util.StringUtils;

/**
 * @author traitswu
 * @date 2021/4/28
 */
public class MappingCacheRegister implements ImportBeanDefinitionRegistrar, EnvironmentAware {
    private static final Logger LOGGER = LoggerFactory.getLogger(MappingCacheRegister.class);

    private AbstractEnvironment environment;

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(importingClassMetadata.getAnnotationAttributes(CCSEnable.class.getName()));
        if (attributes == null) {
            return;
        }
        String[] entityPackages = attributes.getStringArray("entityPackages");
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        for (String entityPackage : entityPackages) {
            try {
                // Here is a special note that the class path must be written in this way.
                // Gets all classes under the specified package
                String basePackage = entityPackage.replace(".", "/");
                Resource[] resources = resourcePatternResolver.getResources("classpath*:" + basePackage + "/*.class");
                MetadataReaderFactory metadataReaderFactory = new SimpleMetadataReaderFactory();
                for (Resource resource : resources) {

                    MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource);
                    String modelClassName = metadataReader.getClassMetadata().getClassName();
                    AnnotationAttributes ccsModelAttrs = AnnotationAttributes.fromMap(metadataReader.getAnnotationMetadata().getAnnotationAttributes(CCSMappingCache.class.getName()));
                    if (ccsModelAttrs == null) {
                        continue;
                    }
                    String topicMaybePlaceHolder = ccsModelAttrs.getString("topic");
                    Class<?> listenerClass = ccsModelAttrs.getClass("listener");
                    String name = ccsModelAttrs.getString("name");
                    String listenerBeanRefName = ccsModelAttrs.getString("listenerBeanRefName");

                    String topic = getTopic(topicMaybePlaceHolder);
                    if (topic == null) {
                        continue;
                    }
                    String beanName = topic;
                    if (StringUtils.hasText(name)) {
                        beanName = name;
                    }

                    GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
                    beanDefinition.setBeanClass(MappingCache.class);
                    beanDefinition.getConstructorArgumentValues().addIndexedArgumentValue(0, topic);
                    beanDefinition.getConstructorArgumentValues().addIndexedArgumentValue(1, modelClassName, "java.lang.Class");
                    beanDefinition.getConstructorArgumentValues().addIndexedArgumentValue(2,
                            StringUtils.hasText(listenerBeanRefName) ? new RuntimeBeanNameReference(listenerBeanRefName) : listenerClass.newInstance()
                    );

                    registry.registerBeanDefinition(beanName, beanDefinition);
                    LOGGER.debug("Register ccs MappingCache {}/{}/{}", beanName, modelClassName, listenerClass.getName());
                }

            } catch (Exception e) {
                LOGGER.error("Register ccs mapping cache failed:", e);
            }
        }
    }

    private String getTopic(String topicMaybePlaceHolder) {
        try {
            PropertySourcesPropertyResolver resolver = new PropertySourcesPropertyResolver(environment.getPropertySources());
            return resolver.resolveRequiredPlaceholders(topicMaybePlaceHolder);
        } catch (IllegalArgumentException e) {
            LOGGER.debug("GetTopic[{}] fail: {}", topicMaybePlaceHolder, ExceptionUtils.getMessage(e));
            return null;
        }
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = (AbstractEnvironment) environment;
    }

}
