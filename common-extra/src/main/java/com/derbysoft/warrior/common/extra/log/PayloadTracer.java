package com.derbysoft.warrior.common.extra.log;

public interface PayloadTracer {

    void trace(Object[] arguments, Object result);

}
