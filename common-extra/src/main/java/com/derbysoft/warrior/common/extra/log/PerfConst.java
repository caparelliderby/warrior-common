package com.derbysoft.warrior.common.extra.log;

public interface PerfConst {

    String UNKNOWN = "Unknown";
    String GET_AVAILABILITY = "GetAvailability";
    String LIVE_CHECK = "LiveCheck";
    String CHECK_QUEUE_SIZE = "CheckQueueSize";
    String CHECK_SIZE = "CheckSize";
    String ARI_REQUEST = "ARIRequest";
    String ARI_ACK = "ARIAck";
    String BATCH_ARI_REQUEST = "BatchARIRequest";
    String SINGLE_CDS_FETCH = "SingleCDSFetch";
    String SUPPLIER_REQUEST = "SupplierRequest";
    String SINGLE_ARI_PROCESS = "SingleARIProcess";
    String BATCH_ARI_PROCESS = "BatchARIProcess";
    String BOOK = "Book";
    String MODIFY = "Modify";
    String CANCEL = "Cancel";
    String HOLD = "Hold";
    String COMMIT = "Commit";
    String MODIFY_HOLD = "ModifyHold";
    String MODIFY_COMMIT = "ModifyCommit";
    String IGNORE = "Ignore";
    String BOOK_NOTIFY = "BookNotify";
    String MODIFY_NOTIFY = "ModifyNotify";
    String CANCEL_NOTIFY = "CancelNotify";
    String PREVIEW = "Preview";
    String QUERY_RES = "QueryRes";
    String POST_CHECK = "PostCheck";
    String ROLLBACK = "Rollback";
    String PAY = "Pay";
    String REFUND = "Refund";
    String PAY_QUERY = "PayQuery";
    String REFUND_QUERY = "RefundQuery";
    String PAY_NOTIFY = "PayNotify";
    String REFUND_NOTIFY = "RefundNotify";
    String SYNC_NOTIFY = "SyncNotify";
    String ASYNC_NOTIFY = "AsyncNotify";
    String SAVE_SENSITIVE_DATA = "SaveSensitiveData";
    String GET_SENSITIVE_DATA = "GetSensitiveData";
    String POP_KEY = "PopKey";
    String GET_CHANGES = "GetChanges";
    String REPORT_SUCCESS = "ReportSuccess";
    String REPORT_FAIL = "ReportFail";
    String GET_KEY = "GetKey";
    String HOTEL_CHANGE = "HotelChange";
    String LOS_RATE_CHANGE = "LOSRateChange";
    String DAILY_RATE_CHANGE = "DailyRateChange";
    String INVENTORY_CHANGE = "InventoryChange";
    String AVAIL_STATUS_CHANGE = "AvailStatusChange";
    String GET_CHANGE = "GetChange";
    String GET_AVAILABILITY_CHANGE = "GetAvailabilityChange";
    String GET_DAILY_CHANGE = "GetDailyChange";
    String SYNC_PROFILE = "SyncProfile";
    String SYNC_CCS = "SyncCCS";
    String SYNC_CURRENCY = "SyncCurrency";
    String API_CALL = "APICall";
    String LOS_RETRIEVE = "LOSRetrieve";
    String DAILY_RETRIEVE = "DailyRetrieve";
    String LOS_CHANGE_RETRIEVE = "LOSChangeRetrieve";
    String DAILY_CHANGE_RETRIEVE = "DailyChangeRetrieve";
    String BOOK_ACK = "BookAck";
    String MODIFY_ACK = "ModifyAck";
    String CANCEL_ACK = "CancelAck";
    String BOOK_REPORT = "BookReport";
    String MODIFY_REPORT = "ModifyReport";
    String CANCEL_REPORT = "CancelReport";
    String HOLD_REPORT = "HoldReport";
    String COMMIT_REPORT = "CommitReport";
    String MODIFY_HOLD_REPORT = "ModifyHoldReport";
    String MODIFY_COMMIT_REPORT = "ModifyCommitReport";
    String IGNORE_REPORT = "IgnoreReport";
    String ROLLBACK_REPORT = "RollbackReport";
    String RES_PAYMENT = "ResPayment";
    String GET_PRODUCT = "GetProduct";
    String ADD_HOTEL = "AddHotel";
    String MODIFY_HOTEL = "ModifyHotel";
    String DELETE_HOTEL = "DeleteHotel";
    String ADD_ROOM = "AddRoom";
    String MODIFY_ROOM = "ModifyRoom";
    String DELETE_ROOM = "DeleteRoom";
    String ADD_RATE_PLAN = "AddRatePlan";
    String MODIFY_RATE_PLAN = "ModifyRatePlan";
    String DELETE_RATE_PLAN = "DeleteRatePlan";
    String LOS_ARI_CHANGE = "LOSARIChange";
    String HOTEL_CHANGE_DATES = "HotelChangeDates";
    String CREATE_TASKS = "CreateTasks";
    String POP_TASKS = "PopTasks";
    String UPDATE_TASK_STATUS = "UpdateTaskStatus";
    String TRIGGER_ARI_REFRESH = "TriggerARIRefresh";
    String POLICY_PROCESS = "PolicyProcess";
    String SYNCER_ARI_PROCESS = "SyncerARIProcess";
    String DISTRIBUTOR_ONLINE_HOTELS = "DistributorOnlineHotels";
    String SUPPLIER_ONLINE_HOTELS = "SupplierOnlineHotels";
    String HBC_REQUEST = "HBCRequest";

    interface Result {
        String SUCCESS = "Success";
        String FAIL = "Fail";
    }

    interface Parameter {
        String PROCESS = "process";

        String HOTEL = "hotel";
        String ROOM_TYPE = "room.type";
        String RATE_PLAN = "rate.plan";

        String AVAILABILITY_RESULT = "availability.result";
        String PROCESS_SUPPLIER = "process.supplier";
        String PROCESS_ORIGINAL = "process.original";
        String PROCESS_DURATION = "process.duration";
        String PROCESS_RESULT = "process.result";

        String RES_ID_DERBY = "res.id.derby";
        String RES_ID_SUPPLIER = "res.id.supplier";
        String RES_ID_CHANNEL = "res.id.channel";
        String CANCEL_CONFIRM_ID = "cancel.confirm.id";

        String SUPPLIER = "supplier";
        String CHANNEL = "channel";

        String HOTEL_SUPPLIER = "hotel.supplier";
        String HOTEL_DERBY = "hotel.derby";
        String HOTEL_CHANNEL = "hotel.channel";

        String ROOM_TYPE_SUPPLIER = "room.type.supplier";
        String ROOM_TYPE_DERBY = "room.type.derby";
        String ROOM_TYPE_CHANNEL = "room.type.channel";

        String RATE_PLAN_SUPPLIER = "rate.plan.supplier";
        String RATE_PLAN_DERBY = "rate.plan.derby";
        String RATE_PLAN_CHANNEL = "rate.plan.channel";

        String ECHO_TOKEN = "echo.token";
        String ECHO_TOKEN_CHANNEL = "echo.token.channel";
        String ECHO_TOKEN_DERBY = "echo.token.derby";

        String CHECK_IN = "check.in";
        String CHECK_OUT = "check.out";
        String START_DATE = "start.date";
        String END_DATE = "end.date";
        String REQUEST_TYPE = "request.type";

        String ADULT_COUNT = "adult.cnt";
        String CHILD_COUNT = "children.cnt";
        String CHILDREN_AGES = "children.ages";

        String ROOM_COUNT = "room.cnt";
        String LOS = "los";
        String IATA = "iata";
        String GUARANTEE_TYPE = "guarantee.type";
        String CARD_CODE = "card.code";

        String BOOK_CNT = "book.cnt";
        String cancel_cnt = "cancel.cnt";
        String modify_cnt = "modify.cnt";

        String MESSAGE = "message";
        String MESSAGE_DURATION_MIN = "message.duration.min";
        String MESSAGE_DURATION_MAX = "message.duration.max";
        String MESSAGE_DURATION_AVG = "message.duration.avg";
        String WAIT_DURATION = "wait.duration";
        String WAIT_QUEUE_SIZE = "wait.queue.size";
        String PROVIDER_CODE = "provider.code";
        String CURRENCY_CODE = "currency.code";
        String PAYMENT_STATUS = "payment.status";

        String ERR_CODE = "err.code";
        String ERR_MSG = "err.msg";

        String ERR_CODE_SUPPLIER = "err.code.supplier";
        String ERR_MSG_SUPPLIER = "err.msg.supplier";
        String ERR_CODE_CHANNEL = "err.code.channel";
        String ERR_MSG_CHANNEL = "err.msg.channel";

        String ERR_SOURCE = "err.source";
        String ERR_CODE_INNER_SERVICE = "err.code.inner.service";

        String WARN_CODE = "warn.code";
        String WARN_MSG = "warn.msg";
    }

}
