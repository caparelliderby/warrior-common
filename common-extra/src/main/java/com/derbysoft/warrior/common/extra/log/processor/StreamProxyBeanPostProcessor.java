package com.derbysoft.warrior.common.extra.log.processor;

import com.derbysoft.warrior.common.extra.log.StreamBeanConfig;
import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.RegexpMethodPointcutAdvisor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author traitswu
 * @date 2021/3/18
 */
public class StreamProxyBeanPostProcessor implements BeanPostProcessor {

    private List<StreamBeanConfig> streamBeanConfigs = new ArrayList<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Optional<StreamBeanConfig> streamDescription = getStreamDescription(beanName);
        return streamDescription.map(streamBeanConfig -> createStreamProxy(bean, streamBeanConfig)).orElse(bean);
    }

    private Object createStreamProxy(Object bean, StreamBeanConfig streamDescription) {
        RegexpMethodPointcutAdvisor advisor = new RegexpMethodPointcutAdvisor();
        advisor.setPatterns(streamDescription.getMethodNames());
        advisor.setAdvice((MethodInterceptor) invocation -> {
            Object[] arguments = null;
            Object result = null;
            try {
                arguments = invocation.getArguments();
                result = invocation.proceed();
                return result;
            } finally {
                if (streamDescription.getPayloadTracer() != null) {
                    streamDescription.getPayloadTracer().trace(arguments, result);
                }
            }
        });
        ProxyFactory proxyFactory = new ProxyFactory(bean);
        proxyFactory.addAdvisor(advisor);
        proxyFactory.setProxyTargetClass(true);
        return proxyFactory.getProxy();
    }

    private Optional<StreamBeanConfig> getStreamDescription(String beanName) {
        return streamBeanConfigs.stream()
                .filter(streamDescription -> beanName.equals(streamDescription.getBeanName()))
                .findFirst();
    }

    public void setStreamBeanConfigs(List<StreamBeanConfig> streamBeanConfigs) {
        this.streamBeanConfigs = streamBeanConfigs;
    }
}
