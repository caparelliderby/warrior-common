package com.derbysoft.warrior.common.extra.log.processor;

import com.derbysoft.warrior.common.extra.log.PayloadTracer;
import com.derbysoft.warrior.common.extra.log.PerfBeanConfig;
import com.derbysoft.warrior.common.extra.log.PerfTemplate;
import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.NameMatchMethodPointcutAdvisor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author traitswu
 * @date 2021/3/18
 */
public class PerfProxyBeanPostProcessor implements BeanPostProcessor {

    private List<PerfBeanConfig> perfBeanConfigs = new ArrayList<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Optional<PerfBeanConfig> perfDescription = getPerfDescription(beanName);
        return perfDescription.map(perfBeanConfig -> createPerfProxy(bean, perfBeanConfig)).orElse(bean);
    }

    private Object createPerfProxy(Object bean, PerfBeanConfig perfDescription) {
        NameMatchMethodPointcutAdvisor advisor = new NameMatchMethodPointcutAdvisor();
        advisor.setMappedNames(perfDescription.getMethodNames());
        advisor.setAdvice((MethodInterceptor) invocation -> {
            String name = invocation.getMethod().getName();
            return new PerfTemplate(perfDescription.getAction(name), perfDescription.getPerfErrorTracer(name)).invoke(() -> {
                Object[] arguments = null;
                Object result = null;
                try {
                    arguments = invocation.getArguments();
                    result = invocation.proceed();
                    return result;
                } finally {
                    PayloadTracer payloadTracer = perfDescription.getPayloadTracer(name);
                    if (payloadTracer != null) {
                        payloadTracer.trace(arguments, result);
                    }
                }
            });
        });

        ProxyFactory proxyFactory = new ProxyFactory(bean);
        proxyFactory.addAdvisor(advisor);
        proxyFactory.setProxyTargetClass(true);
        return proxyFactory.getProxy();
    }

    private Optional<PerfBeanConfig> getPerfDescription(String beanName) {
        return perfBeanConfigs.stream()
                .filter(perfDescription -> beanName.equals(perfDescription.getBeanName()))
                .findFirst();
    }

    public void setPerfBeanConfigs(List<PerfBeanConfig> perfBeanConfigs) {
        this.perfBeanConfigs = perfBeanConfigs;
    }
}
