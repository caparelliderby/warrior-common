package com.derbysoft.warrior.common.extra.log;

import com.derbysoft.common.util.perflog.v2.PerfLogBuilder;

import java.util.Map;

public class PerfV2MessageBuilder extends PerfV1MessageBuilder {

    @Override
    protected String format(Map<String, Object> messages) {
        PerfLogBuilder builder = new PerfLogBuilder();
        for (Map.Entry<String, Object> entry : messages.entrySet()) {
            builder.append(entry.getKey(), entry.getValue());
        }
        return builder.toString();
    }
}
