package com.derbysoft.warrior.common.extra.config.ccs;

import com.derbysoft.ccs.core.MappingCache;
import com.derbysoft.ccs.core.TopicConsumer;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import java.util.List;

/**
 * @author traitswu
 * @date 2021/4/28
 */

@Configuration
@ConditionalOnClass(MappingCache.class)
@ConditionalOnProperty(prefix = "ccs", name = "url")
@ConditionalOnExpression("environment.getProperty('ccs.url', '').trim().length()>0")
@Order
public class CCSTopicConsumerConfiguration {

    @Value("${ccs.consumer.id}")
    private String consumerId;

    @Value("${ccs.zookeeper.connect}")
    private String zookeeperConnect;

    @Value("${ccs.cache.file.dir}")
    private String cacheFileDir;

    @Value("${ccs.url}")
    private String configCenterUrl;

    @Bean
    @ConditionalOnMissingBean
    public TopicConsumer newTopicConsumer(List<MappingCache> caches) throws Exception {
        return new TopicConsumer(
                this.consumerId,
                this.zookeeperConnect,
                this.cacheFileDir,
                this.configCenterUrl,
                Lists.newArrayList(caches)
        );
    }
}
