package com.derbysoft.warrior.common.extra.log.advisor;

import com.derbysoft.warrior.common.extra.log.PayloadTracer;
import com.derbysoft.warrior.common.extra.log.ReflectUtils;
import com.derbysoft.warrior.common.extra.log.Stream;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * @author traitswu
 * @date 2021/3/18
 */
public class StreamInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Stream stream = ReflectUtils.getMethodAnnotation(invocation, Stream.class);
        if (stream == null) {
            return invocation.proceed();
        }
        Object[] arguments = null;
        Object result = null;
        try {
            arguments = invocation.getArguments();
            result = invocation.proceed();
            return result;
        } finally {
            PayloadTracer payloadTracer = ReflectUtils.newInstance(stream.payloadTracer());
            if (payloadTracer != null) {
                payloadTracer.trace(arguments, result);
            }
        }
    }
}
