package com.derbysoft.warrior.common.extra.config.ccs;

import com.derbysoft.ccs.core.MappingCacheListener;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.List;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author traitswu
 * @date 2021/4/28
 */
@Documented
@Target(TYPE)
@Retention(RUNTIME)
public @interface CCSMappingCache {

    String topic();

    String name();

    Class<? extends MappingCacheListener> listener() default NOPMappingCacheListener.class;

    String listenerBeanRefName() default "";


    class NOPMappingCacheListener<T> implements MappingCacheListener<T> {

        @Override
        public void onReset(List oldList, List newList) {

        }

        @Override
        public void onUpdate(Object oldObj, Object newObj) {

        }

        @Override
        public void onDelete(Object obj) {

        }

        @Override
        public void onDisable(Object obj) {

        }

        @Override
        public void onEnable(Object obj) {

        }
    }
}
