package com.derbysoft.warrior.common.extra.log;

import com.derbysoft.dswitch.remote.hotel.exception.AbstractCodeException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultPerfErrorTracer implements PerfErrorTracer {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPerfErrorTracer.class);

    @Override
    public void trace(Throwable throwable) {
        try {
            if (throwable != null) {
                if (throwable instanceof AbstractCodeException) {
                    AbstractCodeException codeException = (AbstractCodeException) throwable;
                    PerfTracer.get().message(PerfConst.Parameter.ERR_CODE, codeException.getCode());
                    PerfTracer.get().message(PerfConst.Parameter.ERR_SOURCE, codeException.getErrorSource());
                    PerfTracer.get().message(PerfConst.Parameter.ERR_MSG, codeException.getErrorMessage());
                } else {
                    PerfTracer.get().message(PerfConst.Parameter.ERR_CODE, "Unknown");
                    PerfTracer.get().message(PerfConst.Parameter.ERR_MSG, ExceptionUtils.getMessage(throwable));
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Record perf failed:", e);
        }
    }
}
