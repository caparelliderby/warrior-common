package com.derbysoft.warrior.common.extra.log;


public class PerfTemplate {

    private final String action;
    private final PerfErrorTracer errorTracer;

    public PerfTemplate(String action) {
        this(action, null);
    }

    public PerfTemplate(String action, PerfErrorTracer errorTracer) {
        this.action = action;
        this.errorTracer = errorTracer;
    }

    public <T> T invoke(Callable<T> callable) throws Throwable {
        boolean isNewPerf = false;
        PerfTracer tracer;
        if (PerfTracer.isExist()) {
            tracer = PerfTracer.get();
        } else {
            isNewPerf = true;
            tracer = new PerfTracer(action);
        }

        if (isNewPerf) {
            tracer.start();
            tracer.result(PerfConst.Result.SUCCESS);
        }
        try {
            return callable.call();
        } catch (Throwable e) {
            tracer.result(PerfConst.Result.FAIL);
            if (errorTracer != null) {
                errorTracer.trace(e);
            }
            throw e;
        } finally {
            if (isNewPerf) {
                tracer.end();
                tracer.record();
            }
        }
    }

    /**
     * For Throwable　Error exception(Jvm Error), wrap to runtime exception is no negative effect.
     * Jvm only have one not daemon thread，jvm will not be quit.
     *
     * @param runnable action
     */
    public void invoke(Runnable runnable) {
        try {
            invoke((Callable<Void>) () -> {
                runnable.run();
                return null;
            });
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

}
