package com.derbysoft.warrior.common.extra.log;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultPerfLogRecorder implements PerfLogRecorder {

    private final PerfMessageBuilder messageBuilder;
    private final Logger logger;

    public DefaultPerfLogRecorder() {
        this(new PerfV1MessageBuilder(), LOGGER_NAME_PERF);
    }

    public DefaultPerfLogRecorder(PerfMessageBuilder messageBuilder, String loggerName) {
        this(messageBuilder, LoggerFactory.getLogger(loggerName));
    }

    public DefaultPerfLogRecorder(PerfMessageBuilder messageBuilder, Logger logger) {
        super();
        this.messageBuilder = messageBuilder;
        this.logger = logger;
    }

    @Override
    public String record(PerfTracer tracer) {
        String message = messageBuilder.toString(tracer);
        if (StringUtils.isNotBlank(message)) {
            logger.info(message);
        }
        return message;
    }

}
