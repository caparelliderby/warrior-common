package com.derbysoft.warrior.common.extra.log;

import com.derbysoft.warrior.common.extra.log.advisor.MethodPointcut;
import com.derbysoft.warrior.common.extra.log.advisor.PerfInterceptor;
import com.google.common.collect.Lists;
import org.springframework.aop.Advisor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.aop.support.DefaultPointcutAdvisor;

import java.lang.annotation.Annotation;
import java.util.List;

public final class PerfProxyUtils {

    public static <P> P getProxy(P object) {
        Class<? extends Annotation>[] annotations = new Class[]{
                Perf.class,
                PerfKeys.class,
                PerfDuration.class
        };

        DefaultPointcutAdvisor defaultPointcutAdvisor = new DefaultPointcutAdvisor();
        defaultPointcutAdvisor.setAdvice(new PerfInterceptor());
        defaultPointcutAdvisor.setPointcut(new MethodPointcut(annotations));

        List<Advisor> advisors = AopUtils.findAdvisorsThatCanApply(Lists.newArrayList(defaultPointcutAdvisor), object.getClass());
        if (advisors.isEmpty()) {
            return object;
        }

        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setTargetClass(object.getClass());
        proxyFactory.setProxyTargetClass(true);
        proxyFactory.addAdvisors(advisors);
        proxyFactory.setTarget(object);
        return (P) proxyFactory.getProxy();
    }

}
