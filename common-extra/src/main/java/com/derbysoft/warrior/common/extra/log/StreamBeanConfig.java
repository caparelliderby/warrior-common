package com.derbysoft.warrior.common.extra.log;

/**
 * @author traitswu
 * @date 2021/3/18
 */
public class StreamBeanConfig {
    private final String beanName;
    private final String[] methodNames;
    private final PayloadTracer payloadTracer;

    public StreamBeanConfig(String beanName, String[] methodNames, PayloadTracer payloadTracer) {
        this.beanName = beanName;
        this.methodNames = methodNames;
        this.payloadTracer = payloadTracer;
    }

    public String getBeanName() {
        return beanName;
    }

    public String[] getMethodNames() {
        return methodNames;
    }

    public PayloadTracer getPayloadTracer() {
        return payloadTracer;
    }
}
