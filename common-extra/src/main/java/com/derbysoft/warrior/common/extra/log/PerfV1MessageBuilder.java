package com.derbysoft.warrior.common.extra.log;


import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class PerfV1MessageBuilder implements PerfMessageBuilder {

    @Override
    public String toString(PerfTracer tracer) {
        Map<String, Object> messages = new HashMap<>(tracer.messages.size() + 10);
        messages.putAll(tracer.messages);
        messages.put(PerfConst.Parameter.PROCESS, tracer.operation);

        String duration = "Unknown";
        if (tracer.endTimstamp != null && tracer.startTimstamp != null) {
            duration = String.valueOf(tracer.endTimstamp - tracer.startTimstamp);
        }
        messages.put(PerfConst.Parameter.PROCESS_DURATION, duration);
        if (!messages.containsKey(PerfConst.Parameter.PROCESS_RESULT) && tracer.result != null) {
            messages.put(PerfConst.Parameter.PROCESS_RESULT, tracer.result);
        }
        return format(messages);
    }

    protected String format(Map<String, Object> messages) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Object> entry : messages.entrySet()) {
            appendInternal(sb, entry.getKey(), escape(entry.getValue().toString()));
        }
        return sb.toString();
    }

    private static void appendInternal(StringBuilder stringBuilder, Object name, String value) {
        stringBuilder.append("<").append(StringUtils.left(name.toString(), 256)).append("=").append(StringUtils.left(value, 1024)).append("> ");
    }

    private static String escape(String input) {
        String output = StringUtils.replace(input, "\r", "&#13;");
        output = StringUtils.replace(output, "\n", "&#10;");
        output = StringUtils.replace(output, "<", "&lt;");
        output = StringUtils.replace(output, ">", "&gt;");
        output = StringUtils.replace(output, "=", "&eq;");
        return output;
    }

}
