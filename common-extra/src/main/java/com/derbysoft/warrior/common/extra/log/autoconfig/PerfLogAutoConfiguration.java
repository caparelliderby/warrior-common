package com.derbysoft.warrior.common.extra.log.autoconfig;

import com.derbysoft.warrior.common.extra.log.Perf;
import com.derbysoft.warrior.common.extra.log.PerfDuration;
import com.derbysoft.warrior.common.extra.log.PerfKeys;
import com.derbysoft.warrior.common.extra.log.Stream;
import com.derbysoft.warrior.common.extra.log.advisor.MethodPointcut;
import com.derbysoft.warrior.common.extra.log.advisor.PerfInterceptor;
import com.derbysoft.warrior.common.extra.log.advisor.StreamInterceptor;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.annotation.Annotation;

/**
 * @author traitswu
 * @date 2021/4/29
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(prefix = "perf.annotation", name = "enable", havingValue = "true")
public class PerfLogAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
        return defaultAdvisorAutoProxyCreator;
    }

    @Bean(name = "perfPointcutAdvisor")
    public DefaultPointcutAdvisor perfPointcutAdvisor() {
        Class<? extends Annotation>[] annotations = new Class[]{
                Perf.class,
                PerfKeys.class,
                PerfDuration.class
        };
        DefaultPointcutAdvisor defaultPointcutAdvisor = new DefaultPointcutAdvisor();
        defaultPointcutAdvisor.setAdvice(new PerfInterceptor());
        defaultPointcutAdvisor.setPointcut(new MethodPointcut(annotations));
        return defaultPointcutAdvisor;
    }

    @Bean(name = "streamPointcutAdvisor")
    public DefaultPointcutAdvisor streamPointcutAdvisor() {
        Class<? extends Annotation>[] annotations = new Class[]{
                Stream.class
        };
        DefaultPointcutAdvisor defaultPointcutAdvisor = new DefaultPointcutAdvisor();
        defaultPointcutAdvisor.setAdvice(new StreamInterceptor());
        defaultPointcutAdvisor.setPointcut(new MethodPointcut(annotations));
        return defaultPointcutAdvisor;
    }


}
