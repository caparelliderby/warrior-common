package com.derbysoft.warrior.common.extra.adapter.mysql;

import com.mysql.cj.jdbc.ha.StandardLoadBalanceExceptionChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

public class AuroraStandardLoadBalanceExceptionChecker extends StandardLoadBalanceExceptionChecker {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuroraStandardLoadBalanceExceptionChecker.class);

    @Override
    public boolean shouldExceptionTriggerFailover(Throwable ex) {
        if (ex instanceof SQLException) {
            SQLException sqlException = (SQLException) ex;
            LOGGER.debug("SQLException {} {} {} {}",
                    sqlException.getErrorCode(), sqlException.getSQLState(),
                    sqlException.getClass().getName(), sqlException.getMessage());

            if (1290 == sqlException.getErrorCode() &&
                    "HY000".equals(sqlException.getSQLState())) {
                LOGGER.warn("Failover {} {} {} {}",
                        sqlException.getErrorCode(), sqlException.getSQLState(),
                        sqlException.getClass().getName(), sqlException.getMessage());
                return true;
            }
        }
        return super.shouldExceptionTriggerFailover(ex);
    }
}
