package com.derbysoft.warrior.common.extra.adapter.web;

import java.util.Map;
import org.apache.commons.lang3.tuple.Pair;

@Deprecated
public interface HttpSender {
    int getStatusCode(String var1, String var2, Pair<String, String>... var3);

    int getStatusCode(String var1, Map<String, String> var2, Pair<String, String>... var3);

    String getResult(String var1, String var2, Pair<String, String>... var3);

    String getResult(String var1, Map<String, String> var2, Pair<String, String>... var3);

    String getResult(String var1, Map<String, String[]> var2);

    byte[] getResult(String var1, byte[] var2);
}
