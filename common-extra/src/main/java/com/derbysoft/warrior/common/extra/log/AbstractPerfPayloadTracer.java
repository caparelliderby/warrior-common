package com.derbysoft.warrior.common.extra.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractPerfPayloadTracer implements PayloadTracer {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPerfPayloadTracer.class);

    @Override
    public void trace(Object[] arguments, Object result) {
        try {
            if (arguments != null && arguments.length > 0) {
                perf(arguments);
            }
            if (result != null) {
                perf(result);
            }
        } catch (Exception e) {
            LOGGER.warn("Record perf log fail:", e);
        }
    }

    protected void perf(Object[] arguments) {

    }


    protected void perf(Object result) {

    }
}
