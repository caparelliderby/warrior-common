package com.derbysoft.warrior.common.extra.log;

public interface PerfMessageBuilder {

    String toString(PerfTracer tracer);

}
