package com.derbysoft.warrior.common.extra.log;

/**
 * @author traitswu
 * @date 2021/3/18
 */
@FunctionalInterface
public interface Callable<V> {

    V call() throws Throwable;

}
