package com.derbysoft.warrior.common.extra.log;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface PerfDuration {

    String action();

    String target();

}
