package com.derbysoft.warrior.common.extra.config;

import com.derbysoft.common.web.servlet.MonitorServlet;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author traitswu
 * @date 2021/4/28
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnWebApplication
public class MonitorServletAutoConfiguration {

    @Bean
    public ServletRegistrationBean<MonitorServlet> monitorServlet() {
        ServletRegistrationBean<MonitorServlet> bean = new ServletRegistrationBean<>();
        bean.addUrlMappings("/status.ci");
        bean.setServlet(new MonitorServlet());
        return bean;
    }
}
