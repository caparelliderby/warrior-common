package com.derbysoft.warrior.common.extra.config;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Can defined the class in spring.factories
 */
public class ApplicationConfigInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext>, Ordered {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfigInitializer.class);
    private static final String APPLICATION_PROPERTY_PLACEHOLDER_LOCATION = "property.placeholder.location";
    private static final String PREFIX_CLASSPATH = "classpath:";

    private final List<String> propertySources = Lists.newArrayList(
            "classpath:*.yml",
            "classpath:*.yaml",
            "classpath:*.properties"
    );

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        try {
            PathMatchingResourcePatternResolver resolver = new ClassPathMatchingResourcePatternResolver(new DefaultResourceLoader());

            Map<String, Properties> sourceProperties = new HashMap<>();
            String specPlaceHolderLocation = null;
            for (String propertySource : propertySources) {
                Resource[] resources = resolver.getResources(propertySource);
                Properties properties = getProperties(resources);
                String placeHolderLocation = properties.getProperty(APPLICATION_PROPERTY_PLACEHOLDER_LOCATION);
                if (StringUtils.hasText(placeHolderLocation)) {
                    specPlaceHolderLocation = StringUtils.trimAllWhitespace(placeHolderLocation);
                }
                sourceProperties.put(propertySource, properties);
            }
            if (specPlaceHolderLocation == null) {
                return;
            }

            Set<String> specLocations = StringUtils.commaDelimitedListToSet(specPlaceHolderLocation);
            Properties appConfig = new Properties();
            for (String singleLocation : specLocations) {
                Properties hadLoadSourceProperties = sourceProperties.get(singleLocation);
                if (hadLoadSourceProperties != null) {
                    appConfig.putAll(hadLoadSourceProperties);
                } else {
                    Resource[] resources = resolver.getResources(singleLocation);
                    appConfig.putAll(getProperties(resources));
                    LOGGER.info("Resolve props[{}] success.", singleLocation);
                }
            }
            applicationContext.getEnvironment().getPropertySources().addLast(new PropertiesPropertySource("application", appConfig));

        } catch (IOException e) {
            throw new RuntimeException("load properties failed:" + ExceptionUtils.getStackTrace(e));
        }
    }

    private Properties getProperties(Resource[] resources) throws IOException {
        Properties properties = new Properties();
        for (Resource resource : resources) {
            properties.putAll(getProperties(resource));
        }
        return properties;
    }

    private Properties getProperties(Resource resource) throws IOException {
        String filename = resource.getFilename();
        if (filename != null && filename.endsWith(".properties")) {
            return PropertiesLoaderUtils.loadProperties(resource);
        }
        if (filename != null && (filename.endsWith(".yml") || filename.endsWith(".yaml"))) {
            YamlPropertiesFactoryBean yamlFactory = new YamlPropertiesFactoryBean();
            yamlFactory.setResources(resource);
            return yamlFactory.getObject();
        }
        throw new IllegalStateException("invalid resource [" + filename + "]");
    }


    private static class ClassPathMatchingResourcePatternResolver extends PathMatchingResourcePatternResolver {

        public ClassPathMatchingResourcePatternResolver(ResourceLoader resourceLoader) {
            super(resourceLoader);
        }

        @Override
        public Resource[] getResources(String locationPattern) throws IOException {
            if (locationPattern.startsWith(PREFIX_CLASSPATH)) {
                if (getPathMatcher().isPattern(locationPattern.substring(PREFIX_CLASSPATH.length()))) {
                    // a class path resource pattern
                    return findPathMatchingResources(locationPattern);
                } else {
                    return findClassPathResources(locationPattern.substring(PREFIX_CLASSPATH.length()));
                }
            }
            return super.getResources(locationPattern);
        }

        protected Resource[] findClassPathResources(String location) throws IOException {
            String path = location;
            if (path.startsWith("/")) {
                path = path.substring(1);
            }
            Set<Resource> result = doFindClassPathResources(path);
            return result.toArray(new Resource[0]);
        }

        protected Set<Resource> doFindClassPathResources(String path) throws IOException {
            Set<Resource> result = new LinkedHashSet<>(16);
            ClassLoader cl = getClassLoader();
            Enumeration<URL> resourceUrls = (cl != null ? cl.getResources(path) : ClassLoader.getSystemResources(path));
            while (resourceUrls.hasMoreElements()) {
                URL url = resourceUrls.nextElement();
                result.add(convertClassLoaderURL(url));
            }
            return result;
        }
    }


    @Override
    public int getOrder() {
        return LOWEST_PRECEDENCE - 10;
    }
}
