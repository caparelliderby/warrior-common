package com.derbysoft.warrior.common.extra.log;

import java.lang.annotation.*;

/**
 * @author traitswu
 * @date 2021/3/18
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Stream {

    Class<? extends PayloadTracer> payloadTracer() default NoOpPayloadTracer.class;

}
