package com.derbysoft.warrior.common.extra.log;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;


public class PerfTracer {

    private static List<PerfLogRecorder> RECORDERS = Lists.newArrayList(new DefaultPerfLogRecorder());
    private static PerfMessageBuilder MESSAGE_BUILDER = new PerfV1MessageBuilder();
    private final Set<String> messageExcludeKeys = Sets.newHashSet(
            PerfConst.Parameter.PROCESS,
            PerfConst.Parameter.PROCESS_DURATION
    );

    final String operation;
    String result;
    Long startTimstamp;
    Long endTimstamp;

    Map<String, Object> messages = Collections.synchronizedMap(new LinkedHashMap<>());

    public PerfTracer(String operation) {
        super();
        this.operation = operation;
    }

    public static void setRecorders(List<PerfLogRecorder> recorders) {
        RECORDERS = CollectionUtils.isEmpty(recorders) ? Lists.newArrayList() : recorders;
    }

    public static void setDefaultMessageBuilder(PerfMessageBuilder builder) {
        if (builder == null) {
            throw new IllegalArgumentException("builder is required");
        }
        MESSAGE_BUILDER = builder;
    }

    public PerfTracer result(String result) {
        this.result = result;
        return this;
    }

    protected PerfTracer start() {
        this.startTimstamp = System.currentTimeMillis();
        Context.set(this);
        return this;
    }


    protected PerfTracer end() {
        this.endTimstamp = System.currentTimeMillis();
        Context.remove();
        return this;
    }

    /**
     * This function is only used for record start timestamp by manual
     *
     * @param startTimestamp
     * @return
     */
    public PerfTracer start(long startTimestamp) {
        this.startTimstamp = startTimestamp;
        return this;
    }

    /**
     * This function is only used for record end timestamp by manual
     *
     * @param endTimestamp
     * @return
     */
    public PerfTracer end(long endTimestamp) {
        this.endTimstamp = endTimestamp;
        return this;
    }

    /**
     * trace a single value with the message key
     */
    public PerfTracer message(Object key, Object value) {
        if (key == null || value == null || StringUtils.isBlank(value.toString()) || messageExcludeKeys.contains(key)) {
            return this;
        }
        this.messages.put(key.toString(), value);
        return this;
    }

    public PerfTracer message(Object key, Object value, Object defaultValue) {
        return this.message(key, value != null ? value : defaultValue);
    }

    public PerfTracer extMessage(Object key, Object value) {
        return this.message("ext." + key, value);
    }

    public PerfTracer reset() {
        this.result = null;
        this.startTimstamp = null;
        this.endTimstamp = null;
        this.messages.clear();
        return this;
    }

    public static PerfTracer get() {
        PerfTracer tracer = Context.get();
        if (tracer != null) {
            return tracer;
        }
        return new PerfTracer("Unkown");
    }

    public static boolean isExist() {
        return Context.get() != null;
    }

    public void record() {
        for (PerfLogRecorder recorder : RECORDERS) {
            recorder.record(this);
        }
    }

    @Override
    public String toString() {
        return MESSAGE_BUILDER.toString(this);
    }

    static class Context {

        private static final ThreadLocal<PerfTracer> THREAD_LOCAL = new ThreadLocal<PerfTracer>();

        public static PerfTracer get() {
            return THREAD_LOCAL.get();
        }

        public static PerfTracer set(PerfTracer tracer) {
            THREAD_LOCAL.set(tracer);
            return tracer;
        }

        public static void remove() {
            THREAD_LOCAL.remove();
        }

    }

}
