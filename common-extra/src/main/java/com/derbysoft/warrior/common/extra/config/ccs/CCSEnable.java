package com.derbysoft.warrior.common.extra.config.ccs;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author traitswu
 * @date 2021/4/28
 */
@Documented
@Target(TYPE)
@Retention(RUNTIME)
@Import({MappingCacheRegister.class, CCSTopicConsumerConfiguration.class})
public @interface CCSEnable {

    String[] entityPackages() default {};
}
