package com.derbysoft.warrior.common.extra.log;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * For declared perf
 */
public class PerfBeanConfig {
    private static final PayloadTracer defaultPayloadTracer = new NoOpPayloadTracer();
    private static final PerfErrorTracer defaultPerfErrorTracer = new DefaultPerfErrorTracer();

    private final String beanName;
    private final List<PerfConfig> perfConfigs;

    public PerfBeanConfig(String beanName) {
        this.beanName = beanName;
        this.perfConfigs = new ArrayList<>();
    }

    private PerfBeanConfig(String beanName, List<PerfConfig> perfConfigs) {
        this.beanName = beanName;
        this.perfConfigs = perfConfigs;
    }

    public PerfBeanConfig(String beanName, String methodName, String action) {
        this(beanName, Lists.newArrayList(new PerfConfig(methodName, action)));
    }


    public void addPointcut(String methodName, String action) {
        this.addPointcut(methodName, action, defaultPayloadTracer, defaultPerfErrorTracer);
    }

    public void addPointcut(String methodName, String action, PayloadTracer payloadTracer) {
        this.addPointcut(methodName, action, payloadTracer, defaultPerfErrorTracer);
    }

    public void addPointcut(String methodName, String action, PayloadTracer payloadTracer, PerfErrorTracer perfErrorTracer) {
        PerfConfig perfConfig = new PerfConfig(methodName, action);
        perfConfig.payloadTracer = payloadTracer;
        perfConfig.perfErrorTracer = perfErrorTracer;
        perfConfigs.add(perfConfig);
    }

    public PerfErrorTracer getPerfErrorTracer(String methodName) {
        return perfConfigs.stream()
                .filter(perfConfig -> perfConfig.methodName.equals(methodName))
                .map(perfConfig -> perfConfig.perfErrorTracer)
                .findFirst()
                .orElse(defaultPerfErrorTracer);
    }

    public PayloadTracer getPayloadTracer(String methodName) {
        return perfConfigs.stream()
                .filter(perfConfig -> perfConfig.methodName.equals(methodName))
                .map(perfConfig -> perfConfig.payloadTracer)
                .findFirst()
                .orElse(defaultPayloadTracer);
    }

    public String getBeanName() {
        return beanName;
    }

    public String[] getMethodNames() {
        return perfConfigs.stream()
                .map(perfConfig -> perfConfig.methodName)
                .toArray(String[]::new);
    }

    public String getAction(String methodName) {
        return perfConfigs.stream()
                .filter(perfConfig -> perfConfig.methodName.equals(methodName))
                .map(perfConfig -> perfConfig.action)
                .findFirst()
                .orElse("Unkown");
    }

    private static class PerfConfig {
        private final String methodName;
        private final String action;
        private PerfErrorTracer perfErrorTracer = defaultPerfErrorTracer;
        private PayloadTracer payloadTracer = defaultPayloadTracer;

        public PerfConfig(String methodName, String action) {
            this.methodName = methodName;
            this.action = action;
        }

    }

}
