package com.derbysoft.warrior.common.extra.log;

import java.lang.annotation.*;


@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Perf {

    /**
     * @return Operation Name
     */
    String action() default PerfConst.UNKNOWN;

    Class<? extends PerfErrorTracer> errorTracer() default DefaultPerfErrorTracer.class;

    Class<? extends PayloadTracer> payloadTracer() default NoOpPayloadTracer.class;

}
