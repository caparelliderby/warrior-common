//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.derbysoft.warrior.common.extra.adapter.web;

import com.derbysoft.common.exception.SystemInternalException;
import com.derbysoft.common.util.ExceptionUtils;
import com.derbysoft.dtrace.httpclient.DTraceHttpClientBuilder;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.*;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.*;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

@Deprecated
public class HttpSenderImpl implements HttpSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpSenderImpl.class);
    private int socketTimeout;
    private int connectionTimeout;
    private int connectionRequestTimeout;
    private X509HostnameVerifier hostnameVerifier;
    private TrustStrategy trustStrategy;
    private KeyStore trustStore;

    public HttpSenderImpl() {
        this.socketTimeout = (int) TimeUnit.SECONDS.toMillis(120L);
        this.connectionTimeout = (int) TimeUnit.SECONDS.toMillis(10L);
        this.connectionRequestTimeout = (int) TimeUnit.SECONDS.toMillis(10L);
        this.hostnameVerifier = new StrictHostnameVerifier();
        this.trustStrategy = (chain, authType) -> {
            return true;
        };
    }

    public int getStatusCode(String url, String request, Pair<String, String>... headers) {
        return this.getStatusCode(this.getHttpClient(url), this.post(url, request, this.createHeaders(headers)));
    }

    public int getStatusCode(String url, Map<String, String> parameters, Pair<String, String>... headers) {
        return this.getStatusCode(this.getHttpClient(url), this.post(url, parameters, this.createHeaders(headers)));
    }

    public String getResult(String url, String request, Pair<String, String>... headers) {
        return this.getContent(this.getHttpClient(url), this.post(url, request, this.createHeaders(headers)));
    }

    public String getResult(String url, Map<String, String> parameters, Pair<String, String>... headers) {
        return this.getContent(this.getHttpClient(url), this.post(url, parameters, this.createHeaders(headers)));
    }

    public String getResult(String url, Map<String, String[]> parameters) {
        return this.getResult(url, this.convertParameter(parameters));
    }

    public byte[] getResult(String url, byte[] request) {
        return this.getResult(url, request, this.getHttpClient(url));
    }

    private Map<String, String> convertParameter(Map<String, String[]> parameters) {
        Map<String, String> map = new HashMap<>();
        Iterator var3 = parameters.entrySet().iterator();

        while (var3.hasNext()) {
            Entry<String, String[]> entry = (Entry) var3.next();
            map.put(entry.getKey(), ArrayUtils.isEmpty((Object[]) entry.getValue()) ? "" : ((String[]) entry.getValue())[0]);
        }

        return map;
    }

    private CloseableHttpClient getHttpClient(String url) {
        return DTraceHttpClientBuilder.tracedHttpClientBuilder(HttpClients.custom()).setSSLSocketFactory(this.isHttps(url) ? this.getSslConnectionSocketFactory() : null).build();
    }

    private boolean isHttps(String url) {
        return StringUtils.startsWith(url, "https");
    }

    private byte[] getResult(String url, byte[] request, CloseableHttpClient httpclient) {
        byte[] var6;
        try {
            HttpPost httpPost = this.post(url, (HttpEntity) (new ByteArrayEntity(request)), (Header[]) null);
            httpPost.setHeader("Content-type", "application/octet-stream");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            var6 = EntityUtils.toByteArray(httpResponse.getEntity());
        } catch (Exception var15) {
            LOGGER.error(ExceptionUtils.toString(var15));
            throw new SystemInternalException(var15);
        } finally {
            try {
                httpclient.close();
            } catch (IOException var14) {
                LOGGER.error(ExceptionUtils.toString(var14));
            }

        }

        return var6;
    }

    private String getContent(CloseableHttpClient httpclient, HttpPost httpPost) {
        String var4;
        try {
            HttpResponse httpResponse = httpclient.execute(httpPost);
            var4 = EntityUtils.toString(httpResponse.getEntity(), Consts.UTF_8);
        } catch (Exception var13) {
            LOGGER.error(ExceptionUtils.toString(var13));
            throw new SystemInternalException(var13);
        } finally {
            try {
                httpclient.close();
            } catch (IOException var12) {
                LOGGER.error(ExceptionUtils.toString(var12));
            }

        }

        return var4;
    }

    private int getStatusCode(CloseableHttpClient httpclient, HttpPost httpPost) {
        int var4;
        try {
            HttpResponse httpResponse = httpclient.execute(httpPost);
            var4 = httpResponse.getStatusLine().getStatusCode();
            return var4;
        } catch (Exception var14) {
            LOGGER.error(ExceptionUtils.toString(var14));
            var4 = HttpStatus.INTERNAL_SERVER_ERROR.value();
        } finally {
            try {
                httpclient.close();
            } catch (IOException var13) {
                LOGGER.error(ExceptionUtils.toString(var13));
            }

        }

        return var4;
    }

    private HttpPost post(String url, String request, Header[] headers) {
        return this.post(url, (HttpEntity) (new StringEntity(request, ContentType.create("text/xml", Consts.UTF_8))), headers);
    }

    private HttpPost post(String url, Map<String, String> parameters, Header[] headers) {
        return this.post(url, this.createParams(parameters), headers);
    }

    private HttpPost post(String url, HttpEntity httpEntity, Header[] headers) {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(httpEntity);
        httpPost.setConfig(this.requestConfig());
        httpPost.setHeaders(headers);
        return httpPost;
    }

    private Header[] createHeaders(Pair<String, String>[] headers) {
        if (headers == null) {
            return null;
        } else {
            List<Header> results = new ArrayList<>();
            Pair[] var3 = headers;
            int var4 = headers.length;

            for (int var5 = 0; var5 < var4; ++var5) {
                Pair<String, String> header = var3[var5];
                results.add(new BasicHeader((String) header.getKey(), (String) header.getValue()));
            }

            return (Header[]) results.toArray(new Header[results.size()]);
        }
    }

    private RequestConfig requestConfig() {
        return RequestConfig.custom().setSocketTimeout(this.getSocketTimeout()).setConnectTimeout(this.getConnectionTimeout()).setConnectionRequestTimeout(this.getConnectionRequestTimeout()).build();
    }

    private HttpEntity createParams(Map<String, String> parameterMap) {
        List<NameValuePair> parameters = new ArrayList<>();
        Iterator var3 = parameterMap.entrySet().iterator();

        while (var3.hasNext()) {
            Entry<String, String> entry = (Entry) var3.next();
            parameters.add(new BasicNameValuePair((String) entry.getKey(), StringUtils.trimToEmpty((String) entry.getValue())));
        }

        return new UrlEncodedFormEntity(parameters, Consts.UTF_8);
    }

    private SSLConnectionSocketFactory getSslConnectionSocketFactory() {
        try {
            SSLContext sslContext = (new SSLContextBuilder()).setSecureRandom(new SecureRandom()).loadTrustMaterial(this.getTrustStore(), this.getTrustStrategy()).build();
            return new SSLConnectionSocketFactory(sslContext, this.getHostnameVerifier());
        } catch (Exception var2) {
            throw new SystemInternalException(var2);
        }
    }

    public int getSocketTimeout() {
        return this.socketTimeout;
    }

    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public int getConnectionTimeout() {
        return this.connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getConnectionRequestTimeout() {
        return this.connectionRequestTimeout;
    }

    public void setConnectionRequestTimeout(int connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
    }

    public X509HostnameVerifier getHostnameVerifier() {
        return this.hostnameVerifier;
    }

    public void setHostnameVerifier(X509HostnameVerifier hostnameVerifier) {
        this.hostnameVerifier = hostnameVerifier;
    }

    public TrustStrategy getTrustStrategy() {
        return this.trustStrategy;
    }

    public void setTrustStrategy(TrustStrategy trustStrategy) {
        this.trustStrategy = trustStrategy;
    }

    public KeyStore getTrustStore() {
        return this.trustStore;
    }

    public void setTrustStore(KeyStore trustStore) {
        this.trustStore = trustStore;
    }
}
