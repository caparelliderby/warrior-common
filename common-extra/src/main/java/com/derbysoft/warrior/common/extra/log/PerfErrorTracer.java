package com.derbysoft.warrior.common.extra.log;

public interface PerfErrorTracer {

    void trace(Throwable throwable);
}
