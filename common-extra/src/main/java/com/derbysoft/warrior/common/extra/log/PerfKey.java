package com.derbysoft.warrior.common.extra.log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({})
public @interface PerfKey {

    byte TARGET_TYPE_PARAMS = 1;
    byte TARGET_TYPE_RETURN = 2;
    byte TARGET_TYPE_CONST = 3;

    String name();

    String index() default "this";

    String constValue() default "";

    byte target() default TARGET_TYPE_PARAMS;

}
