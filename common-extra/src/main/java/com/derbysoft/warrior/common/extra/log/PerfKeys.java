package com.derbysoft.warrior.common.extra.log;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface PerfKeys {

    PerfKey[] value() default {};

    Class<? extends PayloadTracer> payloadTracer() default NoOpPayloadTracer.class;

}
