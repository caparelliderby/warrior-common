package com.derbysoft.warrior.common.extra.log;

import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * @author traitswu
 * @date 2021/3/18
 */
public final class ReflectUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReflectUtils.class);

    public static <T> T newInstance(Class<T> clz) {
        try {
            return clz.newInstance();
        } catch (Exception e) {
            LOGGER.warn("Create instance failed for class  [" + clz.getName() + "] failed", e);
        }
        return null;
    }

    public static <T extends Annotation> T getMethodAnnotation(MethodInvocation invocation, Class<T> clazz) {
        Class<?> targetClass = AopUtils.getTargetClass(Objects.requireNonNull(invocation.getThis()));
        Method method = AopUtils.getMostSpecificMethod(invocation.getMethod(), targetClass);
        return method.getAnnotation(clazz);
    }
}
