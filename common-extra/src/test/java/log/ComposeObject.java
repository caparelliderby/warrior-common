package log;

/**
 * @author traitswu
 * @date 2021/3/19
 */
public class ComposeObject {
    private final String key;
    private final String value;

    public ComposeObject(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
