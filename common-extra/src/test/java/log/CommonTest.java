package log;

import org.apache.commons.beanutils.PropertyUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author traitswu
 * @date 2021/3/19
 */
public class CommonTest {

    @Test
    public void testStringIsPremitive() {
        Assert.assertFalse("".getClass().isPrimitive());
    }

    @Test
    public void testPropertyUtils() throws Exception {

        ComposeObject composeObject = new ComposeObject("abc", "ADE");
        Object key = PropertyUtils.getProperty(composeObject, "key");
        Assert.assertEquals("abc", key);

    }

    @Test
    public void testTestSpringxml(){
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/derbysoft/warrior/common/extra/log/application-perflog.xml");

    }
}
